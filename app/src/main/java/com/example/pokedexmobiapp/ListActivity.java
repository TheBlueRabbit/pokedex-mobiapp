package com.example.pokedexmobiapp;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private ArrayList<Pokemon> listPokemon;
    private RecyclerView pokedex;
    private JSONObject jsonInfos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listPokemon = new ArrayList<Pokemon>();
        pokedex = findViewById(R.id.pokedex);
        pokedex.setLayoutManager(new LinearLayoutManager(this));
        if (isNetworkAvailable()) {
            AsyncPokeDonnees donnees = new AsyncPokeDonnees();
            donnees.execute(getString(R.string.APIURL));
        }



    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class AsyncPokeDonnees extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            URL url = null;
            HttpURLConnection urlConnection = null;
            String result = null;
            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection(); // Open
                InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream

                result = readStream(in); // Read stream

                urlConnection.disconnect();

                jsonInfos = new JSONObject(result);
                JSONArray jsonListPokemon = jsonInfos.getJSONArray("results");

                for (int i = 0; i < 20; i++) {
                    JSONObject jsonPokemon = jsonListPokemon.getJSONObject(i);
                    url = new URL(jsonPokemon.getString("url"));
                    urlConnection = (HttpURLConnection) url.openConnection(); // Open
                    in = new BufferedInputStream(urlConnection.getInputStream()); // Stream

                    String rawInfos = readStream(in);

                    listPokemon.add(new Pokemon(jsonPokemon.getString("name"), rawInfos));

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void v) {
            pokedex.setAdapter(new MyAdapter(listPokemon));


                Button previous = findViewById(R.id.previous);
                if (jsonInfos.isNull("previous")) {
                    previous.setEnabled(false);

                }
                else{
                    previous.setEnabled(true);
                    previous.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                listPokemon.clear();
                                new AsyncPokeDonnees().execute(jsonInfos.getString("previous"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

                Button next = findViewById(R.id.next);
                if (jsonInfos.isNull("next")) {
                    next.setEnabled(false);
                }
                else{
                    next.setEnabled(true);
                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                listPokemon.clear();
                                new AsyncPokeDonnees().execute(jsonInfos.getString("next"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }


        }

        private String readStream(InputStream in) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }
            in.close();
            return sb.toString();
        }
    }


}


