package com.example.pokedexmobiapp;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView nom;
    private ImageView sprite;
    private Pokemon pokemon;


    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        nom = itemView.findViewById(R.id.nom);
        sprite = itemView.findViewById(R.id.sprite);

        itemView.setOnClickListener(this);
    }

    public void bind(Pokemon pokemon) {

        try {
            this.pokemon = pokemon;
            nom.setText(pokemon.getName());
            new DownLoadImageTask(sprite).execute(pokemon.getInfos().getJSONObject("sprites").getString("front_default"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(),DetailsActivity.class);
        intent.putExtra("Pokemon",pokemon.getInfos().toString());
        v.getContext().startActivity(intent);

    }
}
