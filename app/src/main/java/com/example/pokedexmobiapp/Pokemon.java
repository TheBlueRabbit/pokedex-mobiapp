package com.example.pokedexmobiapp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Pokemon implements Serializable {

    private String name;
    private JSONObject infos;

    public Pokemon(String name, String jsonInfos) throws JSONException {
        this.name = name;
        infos = new JSONObject(jsonInfos);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONObject getInfos() {
        return infos;
    }

    public void setInfos(JSONObject infos) {
        this.infos = infos;
    }


}
