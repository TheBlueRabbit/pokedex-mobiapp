package com.example.pokedexmobiapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent listActivity = new Intent(MainActivity.this, ListActivity.class);
        startActivity(listActivity);
    }
}
