package com.example.pokedexmobiapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Bundle bundle = getIntent().getExtras();
        try {
            JSONObject pkmnInfos = new JSONObject(bundle.getString("Pokemon"));

            ((TextView)findViewById(R.id.nom)).setText("Nom: "+pkmnInfos.getString("name"));

            JSONArray jsonTypes = pkmnInfos.getJSONArray("types");
            int size = jsonTypes.length();
            String types = "";
            if (size == 1){
                types += "Type: ";
                types += jsonTypes.getJSONObject(0).getJSONObject("type").getString("name");
            }
            else{
                types += "Types: ";
                types += jsonTypes.getJSONObject(0).getJSONObject("type").getString("name");
                types += " ";
                types += jsonTypes.getJSONObject(1).getJSONObject("type").getString("name");
            }

            ((TextView)findViewById(R.id.type)).setText(types);

            ((TextView)findViewById(R.id.taille)).setText("Taille: "+(double)pkmnInfos.getInt("height")/10+" m");
            ((TextView)findViewById(R.id.poids)).setText("Poids: "+pkmnInfos.getInt("height")*100+" g");

            new DownLoadImageTask((ImageView)findViewById(R.id.image)).execute(pkmnInfos.getJSONObject("sprites").getString("front_default"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void shareText(View view) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBodyText = "Your shearing message goes here";
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject/Title");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(intent, "Choose sharing method"));
    }


}
